Name: BESIII_2021_I1859248
Year: 2021
Summary: Cross section for $e^+e^-\to\phi\Lambda\bar{\Lambda}$ from 3.51 to 4.6 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1859248
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson
References:
 - Phys.Rev.D 104 (2021) 5, 052006
RunInfo: e+ e- to hadrons
Beams: [e+, e-]
Description:
  'Measurement of the cross section for $e^+e^-\to\phi\Lambda\bar{\Lambda}$ from 3.51 to 4.6 GeV by the BESIII collaboration.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021fqx
BibTeX: '@article{BESIII:2021fqx,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Observation of a near-threshold enhancement in the $\Lambda\bar{\Lambda}$ mass spectrum from $e^+e^-\to\phi\Lambda\bar{\Lambda}$ at $\sqrt{s}$ from 3.51 to 4.60 GeV}",
    eprint = "2104.08754",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.104.052006",
    journal = "Phys. Rev. D",
    volume = "104",
    number = "5",
    pages = "052006",
    year = "2021"
}
'
