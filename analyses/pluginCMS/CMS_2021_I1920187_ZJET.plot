# BEGIN PLOT /CMS_2021_I1920187_ZJET/d01-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d03-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d05-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d07-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d09-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d11-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d13-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d15-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d17-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d139-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d140-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d141-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d142-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d143-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d144-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d145-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d192-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d193-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d194-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d195-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d196-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d197-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d198-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d199-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d247-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d248-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d249-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d250-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d251-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d252-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d253-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d254-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d255-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d304-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d305-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d306-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d307-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d308-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d309-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d310-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d311-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d312-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d361-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d362-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d363-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d364-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d365-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d366-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d367-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d368-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d369-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d418-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d419-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d420-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d421-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d422-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d423-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d424-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d425-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d426-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d475-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d476-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d477-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d478-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d479-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d480-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d481-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d482-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d530-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d531-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d532-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d533-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d534-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d535-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d536-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d537-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d585-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d586-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d587-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d588-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d589-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d590-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d591-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d592-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d640-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d641-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d642-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d643-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d644-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d645-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d646-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d647-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d695-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d696-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d697-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d698-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d699-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d700-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d701-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d702-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d750-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d751-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d752-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d753-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d754-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d755-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d756-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d757-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d758-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d807-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d808-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d809-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d810-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d811-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d812-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d813-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d814-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d815-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d864-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d865-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d866-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d867-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d868-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d869-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d870-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d871-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d872-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d921-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d922-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d923-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d924-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d925-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d926-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d927-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d928-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d929-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d978-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d979-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d980-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d981-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d982-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d983-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d984-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d985-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d986-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1035-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1036-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1037-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1038-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1039-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1040-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1041-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1042-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1043-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1092-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1093-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1094-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1095-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1096-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1097-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1098-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1099-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1100-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1149-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1150-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1151-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1152-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1153-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1154-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1155-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1156-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1157-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1206-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1207-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1208-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1209-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1210-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1211-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1212-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1213-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1214-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1263-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1264-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1265-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1266-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1267-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1268-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1269-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1270-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1296-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1297-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1298-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1299-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1300-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1301-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1302-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1303-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1304-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1340-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1341-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1342-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1343-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1344-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1345-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1346-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1347-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1348-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1384-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1385-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1386-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1387-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1388-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1389-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1390-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1391-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1392-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1428-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1429-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1430-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1431-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1432-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1433-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1434-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1435-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1436-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1472-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1473-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1474-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1475-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1476-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1477-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1478-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1479-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1480-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1516-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1517-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1518-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1519-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1520-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1521-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1522-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1523-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1524-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1551-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1552-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1553-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1554-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1555-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1556-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1557-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1558-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1559-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1586-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1587-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1588-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1589-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1590-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1591-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1592-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1593-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1594-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1621-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1622-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1623-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1624-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1625-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1626-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1627-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1628-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1629-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1656-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1657-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1658-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1659-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1660-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1661-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1662-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1663-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1664-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1700-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1701-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1702-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1703-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1704-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1705-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1706-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1707-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1708-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1744-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1745-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1746-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1747-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1748-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1749-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1750-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1751-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1752-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1788-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1789-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1790-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1791-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1792-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1793-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1794-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1795-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1796-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1832-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1833-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1834-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1835-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1836-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1837-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1838-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1839-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1840-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1876-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1877-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1878-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1879-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1880-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1881-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1882-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1883-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1884-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1920-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1921-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1922-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1923-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1924-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1925-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1926-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1927-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1928-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1964-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1965-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1966-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1967-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1968-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1969-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1970-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1971-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d1972-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2008-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2009-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2010-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2011-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2012-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2013-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2014-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2015-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2016-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2052-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2053-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2054-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2055-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2056-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2057-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2058-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2059-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187_ZJET/d2060-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
RatioPlotSameStyle=1
LegendXPos=0.5
# END PLOT
