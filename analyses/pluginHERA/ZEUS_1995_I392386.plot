BEGIN PLOT /ZEUS_1995_I392386/d01-x01-y01
Title=Mean Charged Multiplicity in the Current Fragmentation Region
XLabel=$Q^2$
YLabel=$<N_ch>$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

# ... add more histograms as you need them ...

BEGIN PLOT /ZEUS_1995_I392386/d02-x01-y01
LogX=1
Title=Mean Charged Multiplicity in the Current Fragmentation Region
XLabel=$Q^2$
YLabel=$<N_ch>$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d03-x01-y01
LogX=1
Title=Mean Charged Multiplicity in the Current Fragmentation Region
XLabel=$Q^2$
YLabel=$<N_ch>$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d04-x01-y01
LogX=1
Title=Mean Charged Multiplicity in the Current Fragmentation Region
XLabel=$Q^2$
YLabel=$<N_ch>$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d05-x01-y01
LogX=0
Title=Fitted Values of $\log(1/x_p)$ max
XLabel=$Q^2$
YLabel=$\log(1/x_p)_{max}$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d06-x01-y01
LogX=1
Title=Fitted Values of $\log(1/x_p)$ max
XLabel=$Q^2$
YLabel=$\log(1/x_p)_{max}$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d07-x01-y01
LogX=1
Title=Fitted Values of $\log(1/x_p)$ max
XLabel=$Q^2$
YLabel=$\log(1/x_p)_{max}$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d08-x01-y01
LogX=1
Title=Fitted Values of $\log(1/x_p)$ max
XLabel=$Q^2$
YLabel=$\log(1/x_p)_{max}$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d09-x01-y01
Title=Charged Multiplicity Distributions 
CustomLegend=$6\cdot 10^{-4} < x < 12\cdot 10^{-4}$ \\ $10 < Q^2 < 20 GeV^2$
XLabel=$n_c_h$
YLabel=$1/N dN/dn_c_h$
#XMin=0
##XMax=25
YMin=1e-4
YMax=1
LogX=0
LogY=1
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d10-x01-y01
Title=Charged Multiplicity Distributions  
CustomLegend=$1.2\cdot 10^{-3} < x < 2.4\cdot 10^{-3}$ \\ $10 < Q^2 < 20 GeV^2$
XLabel=$n_c_h$
YLabel=$1/N dN/dn_c_h$
#XMin=0
##XMax=25
YMin=1e-4
YMax=1
LogX=0
LogY=1
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d10-x01-y02
Title=Charged Multiplicity Distributions 
#LegendXPos=0.05
#LegendYPos=0.7
CustomLegend=$6\cdot 10^{-4} < x < 12\cdot 10^{-4}$ \\ $10 < Q^2 < 20 GeV^2$
XLabel=$n_c_h$
YLabel=$1/N dN/dn_c_h$
#XMin=0
##XMax=25
YMin=1e-4
YMax=3
LogX=0
LogY=1
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d10-x01-y03
Title=Charged Multiplicity Distributions 
CustomLegend=$1.2\cdot 10^{-3} < x < 2.4\cdot 10^{-3}$ \\$ 40 < Q^2 < 80 GeV^2$
XLabel=$n_c_h$
YLabel=$1/N dN/dn_c_h$
#XMin=0
##XMax=25
YMin=1e-4
YMax=1
LogX=0
LogY=1
# + any additional plot settings you might like, see make-plots documentation
END PLOT


BEGIN PLOT /ZEUS_1995_I392386/d11-x01-y01
Title=Charged Multiplicity Distributions 
CustomLegend=$2.4\cdot 10^{-3} < x < 20\cdot 10^{-3}$ \\ $20 < Q^2 < 40 GeV^2$
XLabel=$n_c_h$
YLabel=$1/N dN/dn_c_h$
#XMin=0
#XMax=25
YMin=1e-4
YMax=1
LogX=0
LogY=1
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d11-x01-y02
Title=Charged Multiplicity Distributions 
CustomLegend=$2.4\cdot 10^{-3} < x < 20\cdot 10^{-3}$ \\ $40 < Q^2 < 80 GeV^2$
XLabel=$n_c_h$
YLabel=$1/N dN/dn_c_h$
#XMin=0
#XMax=25
YMin=1e-4
YMax=1
LogX=0
LogY=1
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d11-x01-y03
Title=Charged Multiplicity Distributions 
CustomLegend=$2.4\cdot 10^{-3} < x < 20\cdot 10^{-3}$ \\ $80 < Q^2 < 160 GeV^2$
XLabel=$n_c_h$
YLabel=$1/N dN/dn_c_h$
#XMin=0
#XMax=25
YMin=1e-4
YMax=1
LogX=0
LogY=1
LegendYPos=0.6
LegendXPos=0.2
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d11-x01-y04
Title=Charged Multiplicity Distributions 
CustomLegend=$2.4\cdot 10^{-3} < x < 20\cdot 10^{-3}$ \\ $160 < Q^2 < 320 GeV^2$
XLabel=$n_c_h$
YLabel=$1/N dN/dn_c_h$
#XMin=0
#XMax=25
YMin=1e-4
YMax=1
LogX=0
LogY=1
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d12-x01-y01
Title=Charged Multiplicity Distributions 
CustomLegend=$1\cdot 10^{-2} < x < 5\cdot 10^{-2}$ \\ $320 < Q^2 < 640 GeV^2$
XLabel=$n_c_h$
YLabel=$1/N dN/dn_c_h$
#XMin=0
#XMax=25
YMin=1e-4
YMax=1
LogX=0
LogY=1
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d12-x01-y02
Title=Charged Multiplicity Distributions 
CustomLegend=$1\cdot 10^{-2} < x < 5\cdot 10^{-2}$ \\ $640 < Q^2 < 1280 GeV^2$
XLabel=$n_c_h$
YLabel=$1/N dN/dn_c_h$
#XMin=0
#XMax=25
YMin=1e-4
YMax=1
LogX=0
LogY=1
# + any additional plot settings you might like, see make-plots documentation
END PLOT



BEGIN PLOT /ZEUS_1995_I392386/d13-x01-y01
Title=Scaled Momentum distributions 
CustomLegend=$6\cdot 10^{-4} < x < 12\cdot 10^{-4}$ \\ $10 < Q^2 < 20 GeV^2$
XLabel=$\log(1/x_p)$
YLabel=$1/N dN/d\log(1/x_p)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d14-x01-y01
Title=Scaled Momentum distributions  
CustomLegend=$1.2\cdot 10^{-3} < x < 2.4\cdot 10^{-3}$ \\ $10 < Q^2 < 20 GeV^2$
LegendYPos=0.6
LegendXPos=0.2
#YMax =3
XLabel=$\log(1/x_p)$
YLabel=$1/N dN/d\log(1/x_p)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d14-x01-y02
Title=Scaled Momentum distributions 
CustomLegend=$1.2\cdot 10^{-3} < x < 2.4\cdot 10^{-3}$ \\ $Q^2: 20-40GeV^2$
XLabel=$\log(1/x_p)$
YLabel=$1/N dN/d\log(1/x_p)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d14-x01-y03
Title=Scaled Momentum distributions 
CustomLegend=$1.2\cdot 10^{-3} < x < 2.4\cdot 10^{-3}$ \\ $ 40 < Q^2 < 80 GeV^2$
XLabel=$\log(1/x_p)$
YLabel=$1/N dN/d\log(1/x_p)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d15-x01-y01
Title=Scaled Momentum distributions 
CustomLegend=$2.4\cdot 10^{-3} < x < 20\cdot 10^{-3}$ \\ $ 20 < Q^2 < 40 GeV^2$
XLabel=$\log(1/x_p)$
YLabel=$1/N dN/d\log(1/x_p)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d15-x01-y02
Title=Scaled Momentum distributions
CustomLegend=$2.4\cdot 10^{-3} < x < 20\cdot 10^{-3}$ \\ $40 < Q^2 < 80 GeV^2$
XLabel=$\log(1/x_p)$
YLabel=$1/N dN/d\log(1/x_p)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d15-x01-y03
Title=Scaled Momentum distributions 
CustomLegend=$2.4\cdot 10^{-3} < x < 20\cdot 10^{-3}$ \\ $80 < Q^2 < 160 GeV^2$
XLabel=$\log(1/x_p)$
YLabel=$1/N dN/d\log(1/x_p)$
LegendYPos=0.6
LegendXPos=0.3
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d15-x01-y04
Title=Scaled Momentum distributions 
CustomLegend=$2.4\cdot 10^{-3} < x < 20\cdot 10^{-3}$ \\ $160 < Q^2 < 320 GeV^2$
XLabel=$\log(1/x_p)$
YLabel=$1/N dN/d\log(1/x_p)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d16-x01-y01
Title=Scaled Momentum distributions 
CustomLegend=$1\cdot 10^{-2} < x < 5\cdot 10^{-2}$ \\ $320 < Q^2 < 640 GeV^2$
XLabel=$\log(1/x_p)$
YLabel=$1/N dN/d\log(1/x_p)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ZEUS_1995_I392386/d16-x01-y02
Title=Scaled Momentum distributions 
CustomLegend=$1\cdot 10^{-2} < x < 5\cdot 10^{-2}$ \\ $640 < Q^2 < 1280 GeV^2$
XLabel=$\log(1/x_p)$
YLabel=$1/N dN/d\log(1/x_p)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT
